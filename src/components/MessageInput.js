import React from 'react';
import './css/MessageInput.css';
class MessageInput extends React.Component {
    sendMessage = () => {
        this.props.send();
        const input = document.querySelector('.message-input-text');
        input.value = '';
    };
    editMessage = () => {
        const inputArea = document.querySelector('.message-input-text');
        const input = inputArea.value;
        const editedMessage = Object.assign(this.props.editedMessage, {
            text: input,
            createdAt: new Date(),
        });
        this.props.edit(editedMessage);
        inputArea.value = '';
    };
    render() {
        const editing = this.props.editing;
        const onClickFn = !editing ? this.sendMessage : this.editMessage;
        return (
            <div className="message-input">
                <input
                    type="text"
                    className="message-input-text"
                    autoFocus
                ></input>
                <button className="message-input-button" onClick={onClickFn}>
                    {editing ? 'Change' : 'Send'}
                </button>
            </div>
        );
    }
}

export default MessageInput;
