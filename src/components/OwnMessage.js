import React from 'react';
import './css/MessageList.css';
import './css/Message.css';
import './css/OwnMessage.css';
import moment from 'moment';
class OwnMessage extends React.Component {
    onDelete = (e) => {
        this.props.delete(e);
    };
    editMessage = (e) => {
        const input = document.querySelector('.message-input-text');
        input.value =
            e.target.parentElement.querySelector('.message-text').innerText;
        input.focus();
        this.props.edit(e);
    };
    render() {
        let message = this.props.message;
        return (
            <div className="message own-message" id={this.props.id}>
                <p className="message-text">{message.text}</p>
                <p className="message-time">{getTimeHHMM(message.createdAt)}</p>
                <i
                    className="fas fa-pencil-alt message-edit"
                    onClick={this.editMessage}
                ></i>
                <i
                    className="fas fa-trash-alt message-delete"
                    onClick={this.onDelete}
                ></i>
            </div>
        );
    }
}

export const getTimeHHMM = (data) => {
    const format = 'HH:mm';
    var date = new Date(data);
    let dateTime = moment(date).format(format);
    return dateTime;
};
export default OwnMessage;
