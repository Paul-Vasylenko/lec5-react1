import React from 'react';
import Preloader from './Preloader';
import Logo from './Logo';
import MessageInput from './MessageInput';
import MessageList from './MessageList';
import Header from './Header';
import { v4 as uuidv4 } from 'uuid';

class Chat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            messages: [],
            fetched: false,
            editing: false,
            editedMessage: null,
        };
    }
    onSendMsg = () => {
        const id = uuidv4();
        const input = document.querySelector('.message-input-text');
        const messageText = input.value;
        const TDate = new Date();
        const message = {
            id: id,
            text: messageText,
            createdAt: TDate,
            isOwn: true,
        };
        const messages = this.state.messages;
        messages.push(message);
        this.setState({ messages: messages });
    };
    onDeleteMsg = (e) => {
        const messages = this.state.messages.filter((item) => {
            if (item.id !== e.target.parentElement.id) {
                return item;
            }
        });
        this.setState({ messages: messages });
    };
    componentDidMount() {
        fetch(this.props.url)
            .then((res) => res.json())
            .then((json) => {
                json.sort((message1, message2) => {
                    if (message1.createdAt < message2.createdAt) return -1;
                    if (message1.createdAt === message2.createdAt) return 0;
                    if (message1.createdAt > message2.createdAt) return 1;
                });
                this.setState({ messages: json, fetched: true });
            });
    }
    onEditMsg = (e) => {
        const id = e.target.parentElement.id;
        const message = this.state.messages.find((item) => item.id === id);
        this.setState({ editing: true, editedMessage: message });
    };
    editMessage = (editedMessage) => {
        const id = editedMessage.id;
        const message = this.state.messages.find((item) => item.id === id);
        message.text = editedMessage.text;
        message.createdAt = editedMessage.createdAt;
        this.setState({ editing: false, editedMessage: null });
    };
    render = () => {
        const data = this.state;
        const isFetched = this.state.fetched;
        if (!isFetched) {
            return <Preloader />;
        } else {
            return (
                <div className="chat">
                    <Logo />
                    <Header messages={data.messages} />
                    <MessageList
                        messages={data.messages}
                        functions={{
                            delete: this.onDeleteMsg,
                            edit: this.onEditMsg,
                        }}
                    />
                    <MessageInput
                        send={this.onSendMsg}
                        edit={this.editMessage}
                        editing={data.editing}
                        editedMessage={data.editedMessage}
                    />
                </div>
            );
        }
    };
}
export default Chat;
