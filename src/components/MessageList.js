import React from 'react';
import './css/MessageList.css';
import Message from './Message';
import OwnMessage from './OwnMessage';
import Devider from './Devider';
import { v4 as uuidv4 } from 'uuid';

const isDateEqual = (date1, date2) => {
    return (
        new Date(date1).getFullYear() === new Date(date2).getFullYear() &&
        new Date(date1).getDate() === new Date(date2).getDate() &&
        new Date(date1).getMonth() === new Date(date2).getMonth()
    );
};

class MessageList extends React.Component {
    constructor(props) {
        super(props);
        this.state = { messages: props.messages };
        this.lastMessage = null;
    }
    render() {
        return (
            <div className="message-list">
                {this.props.messages.map((message, index, array) => {
                    const toBeReturned = [];
                    if (
                        index === 0 ||
                        !isDateEqual(
                            message.createdAt,
                            array[index - 1].createdAt,
                        )
                    ) {
                        toBeReturned.push(
                            <Devider time={message.createdAt} key={uuidv4()} />,
                        );
                    }
                    message.isOwn
                        ? toBeReturned.push(
                              <OwnMessage
                                  message={message}
                                  key={uuidv4()}
                                  delete={this.props.functions.delete}
                                  edit={this.props.functions.edit}
                                  id={message.id}
                              />,
                          )
                        : toBeReturned.push(
                              <Message
                                  data={{ message }}
                                  key={uuidv4()}
                                  id={message.id}
                              />,
                          );
                    return toBeReturned;
                })}
            </div>
        );
    }
}

export default MessageList;
