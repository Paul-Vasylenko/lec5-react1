import React from 'react';
import './css/Preloader.css';

class Preloader extends React.Component {
    render() {
        return (
            <div className="preloader">
                <div className="spinner"></div>
            </div>
        );
    }
}

export default Preloader;
