import React from 'react';
import './css/MessageList.css';
import moment from 'moment';
import './css/Message.css';

class Message extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLiked: false,
        };
    }
    likeMessage = (e) => {
        e.target.classList.toggle('message-like');
        e.target.classList.toggle('message-liked');
        this.setState({
            isLiked: !this.state.isLiked,
        });
    };
    render() {
        let message = this.props.data.message;
        return (
            <div className="message">
                <p className="message-text">{message.text}</p>
                <p className="message-time">{getTimeHHMM(message.createdAt)}</p>
                <p className="message-user-name">{message.user}</p>
                <img
                    className="message-user-avatar"
                    alt="avatar"
                    src={message.avatar}
                />
                <i
                    className="fas fa-heart message-like"
                    onClick={this.likeMessage}
                ></i>
            </div>
        );
    }
}

const getTimeHHMM = (data) => {
    const format = 'HH:mm';
    var date = new Date(data);
    let dateTime = moment(date).format(format);
    return dateTime;
};
export default Message;
