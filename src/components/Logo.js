import React from 'react';
import './css/Logo.css';
class Logo extends React.Component {
    render() {
        return (
            <div className="logo">
                <img className="logo-img" src="/images/logo.jpg" alt="logo" />
                <span className="logo-text">WebChat</span>
            </div>
        );
    }
}

export default Logo;
